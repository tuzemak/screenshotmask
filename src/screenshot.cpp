#include "screenshot.h"
#include <chrono>
#include <thread>

using namespace std;

constexpr RGBQUAD RGBGREEN = {0x00, 0xff, 0x00, 0x00};

PBITMAPINFO ScreenShooter::CreateBitmapInfo(HBITMAP bitmap) 
{
    BITMAP bmp;
    WORD colorBits;

    GetObject(bitmap, sizeof(BITMAP), &bmp);

    colorBits = bmp.bmPlanes * bmp.bmBitsPixel;
    if (colorBits == 1)
        colorBits = 1;
    else if (colorBits <= 4)
        colorBits = 4;
    else if (colorBits <= 8)
        colorBits = 8;
    else if (colorBits <= 16)
        colorBits = 16;
    else if (colorBits <= 24)
        colorBits = 24;
    else colorBits = 32;


   PBITMAPINFO bitmapInfo;
   if (colorBits == 24) 
   {
      bitmapInfo = (PBITMAPINFO) LocalAlloc(LPTR, sizeof(BITMAPINFOHEADER));
   }
   else
   {
      bitmapInfo = (PBITMAPINFO) LocalAlloc(LPTR, sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * (1U << colorBits));
   }

   bitmapInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
   bitmapInfo->bmiHeader.biWidth = bmp.bmWidth;
   bitmapInfo->bmiHeader.biHeight = bmp.bmHeight;
   bitmapInfo->bmiHeader.biPlanes = bmp.bmPlanes;
   bitmapInfo->bmiHeader.biBitCount = bmp.bmBitsPixel;

   if (colorBits < 24) 
   {
      bitmapInfo->bmiHeader.biClrUsed = (1 << colorBits);
   }

   bitmapInfo->bmiHeader.biCompression = BI_RGB;
   bitmapInfo->bmiHeader.biSizeImage = ((bitmapInfo->bmiHeader.biWidth * colorBits +31) & ~31) /8 * bitmapInfo->bmiHeader.biHeight;
   bitmapInfo->bmiHeader.biClrImportant = 0;
   return bitmapInfo;
}

bool operator!=(const RGBQUAD& lha, const RGBQUAD& rha)
{
   return (lha.rgbBlue != rha.rgbBlue) || 
          (lha.rgbRed != rha.rgbRed) ||
          (lha.rgbBlue != rha.rgbBlue);
}


Screenshot ScreenShooter::MakeScreenShot()
{
   HBITMAP bitmap = CreateCompatibleBitmap(m_desktopContext, m_screenWidth, m_screenHeight);
   SelectObject(m_workingContext, bitmap);
   FillRect(m_workingContext, &m_workingRect, m_brush);
   BitBlt(m_workingContext, 0, 0, m_screenWidth, m_screenHeight, m_desktopContext, 0, 0, SRCCOPY);
   PBITMAPINFO bitmapInfo = CreateBitmapInfo(bitmap);

   return Screenshot{bitmap, bitmapInfo};
}

void ScreenShooter::SaveToFile(LPTSTR filename, Screenshot screenshot)
{

   PBITMAPINFO bitmapInfo = screenshot.m_bitmapInfo;
   PBITMAPINFOHEADER bitmapInfoHeader = &bitmapInfo->bmiHeader;
    
   LPBYTE bitsBuf = (LPBYTE) GlobalAlloc(GMEM_FIXED, bitmapInfoHeader->biSizeImage);

   GetDIBits(m_workingContext, screenshot.m_bitmap, 0, bitmapInfoHeader->biHeight, bitsBuf, bitmapInfo, DIB_RGB_COLORS);

   BITMAPFILEHEADER fileHeader;
   fileHeader.bfType = 0;
   fileHeader.bfType |= 'M' << 8;
   fileHeader.bfType |= 'B';
   fileHeader.bfSize = (sizeof(BITMAPFILEHEADER) + bitmapInfoHeader->biSize + bitmapInfoHeader->biClrUsed * sizeof(RGBQUAD) + bitmapInfoHeader->biSizeImage);
   fileHeader.bfReserved1 = 0;
   fileHeader.bfReserved2 = 0;
   fileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + bitmapInfoHeader->biSize + bitmapInfoHeader->biClrUsed * sizeof (RGBQUAD);

   HANDLE outFile = CreateFile(filename, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
   DWORD bytesWritten = 0;
   WriteFile(outFile, &fileHeader, sizeof(BITMAPFILEHEADER), &bytesWritten,  NULL);
   WriteFile(outFile, bitmapInfoHeader, sizeof(BITMAPINFOHEADER) + bitmapInfoHeader->biClrUsed * sizeof (RGBQUAD), &bytesWritten, NULL);

   DWORD bitsSize = bitmapInfoHeader->biSizeImage;
   WriteFile(outFile, bitsBuf, bitsSize, (LPDWORD) &bytesWritten, NULL);

   CloseHandle(outFile);
   GlobalFree(bitsBuf);
}

void ScreenShooter::MaskBitmap(Screenshot screenshot, Screenshot mask)
{
   PBITMAPINFO bitmapInfo = screenshot.m_bitmapInfo;
   PBITMAPINFOHEADER bitmapInfoHeader = &bitmapInfo->bmiHeader;
    
   LPRGBQUAD bitsBuf = (LPRGBQUAD) GlobalAlloc(GMEM_FIXED, bitmapInfoHeader->biSizeImage);
   GetDIBits(m_workingContext, screenshot.m_bitmap, 0, bitmapInfoHeader->biHeight, bitsBuf, bitmapInfo, DIB_RGB_COLORS);

   bitmapInfo = mask.m_bitmapInfo;
   bitmapInfoHeader = (PBITMAPINFOHEADER) &bitmapInfo->bmiHeader;

   LPRGBQUAD maskBuf = (LPRGBQUAD) GlobalAlloc(GMEM_FIXED, bitmapInfoHeader->biSizeImage);
   GetDIBits(m_workingContext, mask.m_bitmap, 0, bitmapInfoHeader->biHeight, maskBuf, bitmapInfo, DIB_RGB_COLORS);

   for (DWORD idx = 0; idx < bitmapInfoHeader->biSizeImage / sizeof(RGBQUAD); ++idx)
   {
      if (bitsBuf[idx] != maskBuf[idx]) 
      {
         bitsBuf[idx] = RGBGREEN;
      }
   }

   SetDIBits(m_workingContext, screenshot.m_bitmap, 0, bitmapInfoHeader->biHeight, bitsBuf, bitmapInfo, DIB_RGB_COLORS);
   GlobalFree(bitsBuf);
   GlobalFree(maskBuf);
}


int main(int argc, LPTSTR argv[]) {

   ScreenShooter& screenShooter = ScreenShooter::Instance();

   auto screenshot1 = screenShooter.MakeScreenShot();
   screenShooter.SaveToFile((LPTSTR)"screenshot1.bmp", screenshot1);
   
   this_thread::sleep_for(std::chrono::seconds(10));

   auto screenshot2 = screenShooter.MakeScreenShot();
   screenShooter.MaskBitmap(screenshot2, screenshot1);
   screenShooter.SaveToFile((LPTSTR)"screenshot2.bmp", screenshot2);

return 0;
}