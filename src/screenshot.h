#pragma once
#include <windows.h>

bool operator!=(const RGBQUAD& lha, const RGBQUAD& rha);

struct Screenshot
{
    HBITMAP m_bitmap;
    PBITMAPINFO m_bitmapInfo;
};

class ScreenShooter
{
public:
         static ScreenShooter& Instance()
         {
            static ScreenShooter theSingleInstance;
            return theSingleInstance;
         }

         Screenshot MakeScreenShot();
         void SaveToFile(LPTSTR filename, Screenshot screenshot);
         void MaskBitmap(Screenshot screenshot, Screenshot mask);


private:        
      ScreenShooter()
         : m_screenHeight(GetSystemMetrics(SM_CYSCREEN))
         , m_screenWidth(GetSystemMetrics(SM_CXSCREEN))
         , m_desktopContext(GetDC(GetDesktopWindow()))
         , m_workingContext(CreateCompatibleDC(m_workingContext))
         , m_logBrush{ BS_SOLID, 0xFF0000, 0}
         , m_brush(CreateBrushIndirect(&m_logBrush))
         , m_workingRect{0, 0, m_screenWidth, m_screenHeight}
      {}

      ScreenShooter(const ScreenShooter& root) = delete;
      ScreenShooter& operator=(const ScreenShooter&) = delete;

      static PBITMAPINFO CreateBitmapInfo(HBITMAP bitmap);

      int m_screenHeight;
      int m_screenWidth;
      HDC m_desktopContext;
      HDC m_workingContext;
      const LOGBRUSH m_logBrush;
      const HBRUSH m_brush;
      const RECT m_workingRect;
};